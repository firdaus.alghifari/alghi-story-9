from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User

from .views import index

class IndexTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home/index.html')
        
    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 302)
        
    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_hello_url_exists(self):
        response = Client().get('/hello/')
        self.assertEqual(response.status_code, 302)

    def test_wrong_username_message(self):
        response = Client().get('/?failed')
        self.assertIn('Wrong username/password', response.content.decode())

    def test_need_login_message(self):
        response = Client().get('/?needlogin')
        self.assertIn('You need to log in first', response.content.decode())

    def test_logged_out_message(self):
        response = Client().get('/?loggedout')
        self.assertIn('Logout successful', response.content.decode())

    def test_login_logout(self):
        client = Client()

        username = 'kocheng'
        password = 'kocheng123'
        user = User.objects.create_user(username=username, password=password)
        
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        # Test if login successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Selamat datang', response.content.decode())
        
        # Logout
        response = client.get('/logout/')
        
        # Test if logout successful
        response = client.get('/hello/')
        self.assertEqual(response.status_code, 302)


# SELENIUM TEST #
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

class IndexLiveTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(IndexLiveTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(IndexLiveTest, self).tearDown()

    def test_check_title(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)
        self.assertEqual(selenium.title, 'Alghi Story 9')

    def test_bypass_login(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/hello/')
        time.sleep(1)

        self.assertIn('You need to log in first', selenium.page_source)
    
    def test_wrong_login(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)

        elemUsername = selenium.find_element_by_id('inputUsername')
        elemUsername.send_keys('coba')
        elemPassword = selenium.find_element_by_id('inputPassword')
        elemPassword.send_keys('kocheng')
        submitBtn = selenium.find_element_by_css_selector('button[type=submit]')
        submitBtn.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn('Wrong username/password', selenium.page_source)

    def test_login(self):
        selenium = self.selenium

        selenium.get('http://127.0.0.1:8000/')
        time.sleep(1)

        elemUsername = selenium.find_element_by_id('inputUsername')
        elemUsername.send_keys('coba')
        elemPassword = selenium.find_element_by_id('inputPassword')
        elemPassword.send_keys('cobacoba')
        submitBtn = selenium.find_element_by_css_selector('button[type=submit]')
        submitBtn.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn('Selamat datang, Kak Pewe Keren', selenium.page_source)

        logoutBtn = selenium.find_element_by_css_selector('button[type=button]')
        logoutBtn.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn('Logout successful', selenium.page_source)
